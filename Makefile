TARGET=main
PANDOCFLAGGS=-f markdown+raw_tex --pdf-engine=xelatex
NAME ?= regeln
FACH="Umweltbüro Lichtenberg"
WRITTENBY="Umweltbüro Lichtenberg"

include /home/poinck/dateien/config/pandoc/Makefile.in
export

default: $(NAME).pdf wc reminder

clean:
	rm -f *.acn *.acr *.alg *.aux *.bbl *.blg *.dvi *.glg *.glo *.gls *.ist *.lof *.log *.lol *.lot *.out *.pdf *.toc

$(NAME).pdf: $(NAME).md Makefile template.tex
	pandoc $(PANDOCFLAGGS) "$(NAME).md" -o $(NAME).pdf -V papersize:a4 -V fontsize:12pt -V fach=$(FACH) -V writtenby=$(WRITTENBY) -V lang=de $(KP_VARS) --number-sections --template=template.tex
	#--filter=pandoc-citeproc \
  	#--bibliography=/home/poinck/Zotero/_lib/regeln.bib \
	#--csl=/home/poinck/Zotero/_lib/regeln2.csl
	#--verbose
	#--number-sections
	#--toc

wc:
	wc -w "$(NAME).md"
	pdftotext "$(NAME).pdf" - | wc -l
	pdfinfo "$(NAME).pdf" | grep "^Pages:"

reminder:
	@echo -e "\e[1;32mREMINDER\e[0m  aspell check $(NAME).md --mode=tex -l en_EN"

backup:
	cp -vru . /mnt/daten/_sf/Seafile/fs4/rs/${NAME}
