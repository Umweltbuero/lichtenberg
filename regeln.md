\centerline{ }

\vspace{5cm}

\centerline{\Huge \textbf{Spielregeln}}
\vspace{0.8cm}

\centerline{\includegraphics[width=\textwidth,keepaspectratio]{./nl_logo.png}}

\vspace{0.8cm}

\centerline{\large Umweltbüro Lichtenberg, 2022/2023, CC-BY-SA}
\vspace{0.1cm}
\centerline{\large \url{http://umweltbuero-lichtenberg.de/}}

\rhead{}
\rfoot{}
\normalsize

\newpage

\pagenumbering{roman}
\setcounter{page}{2}
\renewcommand{\contentsname}{Inhalt}
\tableofcontents
\rfoot{\small{\textit{\thepage}}}

\rhead{\small{\textit{Inhalt}}}

\newpage

\pagenumbering{arabic}
\setcounter{page}{3}
\lhead{\small{\textit{NEU-Lichtenberg, Spielregeln}}}
\rhead{\small{\textit{\leftmark}}}

# Einleitung und Hintergrund

„NEU-Lichtenberg“ ist ein kooperatives Umweltbildungspiel für Lichtenberg. Ziel ist es die Transformation zu schaffen; d.h. Biodiversität trotz neuer Bedarfe durch Bevölkerungswachstum und Zuzug zu verbessern, eine stabile Grundwasserbilanz zu erreichen und die thermische Belastung zu reduzieren.

Gespielt werden maximal 6 Runden ab 2020. Im Jahr 2095 ist das Spiel vorbei oder wenn eine oder mehrere Kategorien auf dem Transformations-Spielbrett ihren Tiefstwert erreichen. In diesem Fall wäre das Spiel für alle verloren. Bleibt die Position des Transformationsmarkers nach 6 Runden im grünen Bereich ist das Spiel gewonnen und NEU-Lichtenberg kann in eine grüne Zukunft blicken.

Viel Spaß und Erfolg!

# Spielmaterialien

Das Brettspiel besteht aus folgenden Teilen *(Tabelle \ref{mat})*:

\begin{table}[h!]
  \begin{center}
    \caption{Spielmaterialien. $^{a}$Während der Prototypphase sind einige Angaben nur eine Schätzung.}
    \label{mat}
    \small
    \begin{tabular}{rl} % <-- alignments
      \textbf{Menge} & \textbf{Beschreibung} \\
      \hline
      1 & Transformations-Spielbrett \\
      5 & Stadtteil-Spielbretter \\
      5 & Status-Spielbretter mit aufgedruckter Zählhilfe \\
      ca. 40$^{a}$ & Ereigniskarten (rot und grün) \\
      ca. 90$^{a}$ & Projektkarten \\
      ca. 20$^{a}$ & rosa oder pinke Mobilität-Spielsteine (0,7 cm) \\
      ca. 60$^{a}$ & schwarze Wohnflächen-Spielsteine (0,7 cm) \\
      ca. 60$^{a}$ & grüne Vegetation-Spielsteine (0,7 cm) \\
      ca. 20$^{a}$ & gelbe Energie-Spielsteine (0,7 cm) \\
      1 & schwarzer Spielstein für das Level der Versiegelung (1 cm) \\
      1 & grüner Spielstein für das Level der Vegetation (1 cm) \\
      1 & weißer oder blauer Spielstein für das Grundwasserlevel (1 cm) \\
      1 & roter Spielstein für das Level der thermischen Belastung (1 cm) \\
      1 & gelber Spielstein für das Biodiversitätslevel (1 cm) \\
      1 & naturfarbener großer Spielstein für das Transformationslevel \\
       & (beschriftet mit Runden, Jahren und Bevölkerungswachstum) \\
      5 & Spielsteine in beliebiger Farbe für die Markierungen des Planungsbudgets \\
       & ggf. Zettel und Stift für Notizen \\
    \end{tabular}
  \end{center}
\end{table}

## Spielbretter, -steine und -figuren \label{bretter}

Das Spiel kann mit **2** bis **5** Spieler:innen gespielt werden. Für jede Spieler:in gibt es jeweils ein Spielbrett mit 2 bis 3 Stadtteilen des Berliner Bezirks Lichtenberg *(Abb.\ \ref{brett})*. Die Spielbretter können frei gewählt werden, mit Ausnahme des Feldes mit den Stadtteilen Malchow, Wartenberg, Falkenberg und Neu-Hohenschönhausen, welches bei jedem Spiel von einer der Spieler:innen übernommen werden muss[^mwfh]. Jedes Spielbrett hat unterschiedliche Ausgangsvorraussetzungen (Mangel und Überschüsse). Für den Spielerfolg wird aber alles in der Wertungsrunde zusammengerechnet.

[^mwfh]: Wird dieses Spielbrett nicht verwendet, dann wird es unter Umständen sehr schwer die Transformation zu schaffen. Probiert es gerne aus, wenn ihr **\textsf{NEU-Lichtenberg}** schon ein paar mal gewonnen habt.

### Stadtteile \label{stadtteile}

\pic{./startaufstellung1.png}{\textsf{\textbf{NEU-Lichtenberg}}-Stadtteil-Spielbrett mit Startaufstellung für eine Spieler:in.\label{brett}}

Jede Spieler:in hat seine eigenen Spielfelder (*Abb.\ \ref{brett}*, mit aufgebauter Startaufstellung). Ein Feld gilt als versiegelt, wenn sich darauf Bebauung in Form von Wohnfläche *(schwarz)* oder Erzeugung regenerativer Energieen *(gelb)* befinden. Befindet sich zusätzlich noch Vegetation auf dem gleichen Feld, dann gilt dies trotzdem als versiegelt. Felder mit Mobilität (z.\ B. Öffentlicher Personennahverkehr, Auto- und Fahrrad-Sharing) wirken sich positiv auf die Versiegelung aus, weil in diesem Spiel angenommen wird, dass sich dadurch Versiegelung in Form von geteilter Verkehrsinfrastruktur verhindern lässt. Diese können also in der Wertungsphase von der Versiegelung abgezogen werden, auch wenn sie sich zusammen mit Wohnfläche oder Energie auf einem Feld befinden. Auf jedem kleinen Spielfeld (1,5 cm) können maximal 3 Spielsteine platziert werden, auf den größeren (2 cm) bis zu 4 Spielsteine.

Für alle Stadtteil-Spielbretter ist die Startaufstellung mit entsprechend farbig markierten Punkten auf den blau umrandeten Spielfeldern dargestellt.

### Status \label{status} 

Pro Spieler:in gibt es nun noch ein Status-Spielbrett, das das Zählen für die Wertungsphase pro Runde erleichtern soll *(Abb.\ \ref{status})*. In der ersten Zeile $\textsf{(Flächen)}$ können jeweils die Anzahl der versiegelten Spielfelder $\textsf{(VS)}$ und die Felder gezählt werden, die auschließlich durch Vegetation $\textsf{(V)}$ belegt sind. Jedes Feld wird nur einmal gezählt, auch wenn sich darauf unterschiedliche oder gleiche Spielsteine mehrfach befinden. Die Vegetationsfelder werden mit einem grünen und die versiegelten Felder mit einem schwarzen Spielstein gezählt. Gibt es mehr als 10, dann kann dies durch einen weiteren Spielstein mit der entsprechenden Farbe auf dem Status-Spielbrett markiert werden (für mehr als 20 enstprechend).

\pic{./status_spielbrett.png}{\textsf{\textbf{NEU-Lichtenberg}}-Status-Spielbrett für eine Spieler:in.\label{status}}

In der darauffolgenden Zeile $\textsf{(Bebauung)}$ geht es um die Anzahl der Spielsteine auf dem eigenen Stadtteil-Spielbrett. Auch hier werden mit Blick auf die Wertungsphase die Wohnflächen-Spielsteine und Vegetations-Spielsteine gezählt. Hinweis: Hier wird Vegetation auch gezählt, wenn sie zusammen Energieerzeugung oder Wohnfläche auf einem Feld auftaucht. Die Markierung auf dem Status-Spielbrett folgt der wie in der ersten Zeile.

### Transformationslevel \label{trans}

Das Transformations-Spielbrett dient dazu euren Fortschritt während des Spiels festzustellen *(Abb.\ \ref{trans1})*. In der ersten Reihe wird euer aktuelles Transformationslevel mit dem großen Würfel markiert. Auf jeder Seite des Würfels ist eine andere Runde mit Jahreszahl in blau und die zu erwartende Bevölkerung in rot aufgedruckt. In der ersten Runde steht der Würfel aud **$\textsf{1}$**, zeigt das Jahr 2020 und eine Bevölkerung von **\textsf{7}** an.

\pic{./transformation1.jpg}{\textsf{\textbf{NEU-Lichtenberg}}-Tansformations-Spielbrett mit beispielhafter Startaufstellung nach der Wertungsphase vor der ersten Runde für ein 2-Spieler:innen-Spiel für den mittleren Schwierigkeitsgrad.\label{trans1}}

<!--
sodass am aber nach Abzug noch mindestens 5 Planungsressourcen übrig bleiben (Beispiel: Runde 5: Bevölkerung 12, Wohnflächen 10, Planungsbudget wird um 2 auf 8 reduziert).
-->

In den 5 Reihen darunter wird der Transformationsstatus in 5 Kategorien (Versiegelung, Vegetation, Grundwasser, Thermische Belastung und Biodiversität) eingeteilt, der jeweils von 5 (sehr gut) und 0 (sehr schlecht) sein kann. Für jede Kategorie wird jeweils ein anders farbiger Spielstein verwendet um den aktuellen Status zu markieren. Welche Werte diese einnehmen wird in der Wertungsphase mithilfe der Zählhilfe bestimmt *(Kapitel\ \ref{wertung})*. Wo genau der Würfel zum Markieren des aktuellen Transformationslevels (erste Reihe) liegt, hängt vom gewählten Schwierigkeitsgrad und von den Werten der 5 Kategorien ab:

- **sehr schwer:** niedrigste Position einer der 5 Kategorien bestimmt das Transformationslevel.
- **schwer:** 2. niedrigstee Position bestimmt das Transformationslevel.
- **mittel (Empfehlung):** 3. niedrigste Position.
- **leicht:** 4. niedrigste.
- **sehr leicht:** 5. niedrigste (Sandkastenstufe).

## Ereigniskarten

Ereigniskarten bestimmen wiederkehrende zufällige oder einmalige Ereignisse, die den Spielverlauf (alle Spielphasen) beeinflussen. Je nach Transformationslevel werden unterschiedliche viele grüne und rote Ereigniskarten gezogen *(siehe Kapitel \ref{ereignisse})*.

\pic{./ep2.pdf}{Grüne Ereigniskarte\label{eplus}}

<!--\pic{./ereignis_descr.png}{Rück- und Vorderseite einer Ereigniskarte\label{edescr}}-->

\hyphenation{Planungs-ressourcen}

*Abb.\ \ref{eplus}* zeigt eine grüne Ereigniskarte, die sich positiv auf den Planungsauwand auswirkt. Auswirkungen auf den Planungsaufwand oder die eigenen Planungsressourcen werden immer oben rechts blau unterlegt dargestellt und auf der Karte genauer beschrieben. Auf der gezeigten Karte gilt der reduzierte Planungsaufwand für Projekte die Streuobstwiesen umsetzen. Das Symbol in der Mitte wird auf jeder Karte beschrieben. Hier verdeutlicht es, dass Vegetation auf einem Feld ohne weitere Bebauung durch verringerten Planungsaufwand unterstützt wird.

## Projektkarten

Projektkarten stehen für Projektvorhaben, die während der Planungsphase für **1** Planungsaufwand pro Karte behalten *(siehe Kapitel \ref{projekte})* und in der Umsetzungsphase zum aufgedruckten Planungsaufwand ausgespielt werden können *(siehe Kapitel \ref{umsetzung})*.

\pic{./p2.pdf}{Projektkarte\label{p}}

<!--\pic{./projekt_descr.png}{Rück- und Vorderseite einer Projektkarte\label{pdescr}}-->

*Abb.\ \ref{p}* zeigt eine Projektkarte die es ermöglicht für **6** Planungsressources neuen Wohnraum zusammen mit Stadtgrün und Energieerzeugung auf 2 freien Feldern zu bauen. Auch hier ist oben rechts blau unterlegt der Planungsaufwand auf allen Projektkarten darfestellt, der bei der Umsetzung aufgewendet werden muss. Das Symbol in der Mitte deutet mit dem **\textsf{+}** an, dass auf dem Spielbrett Spielsteine gesetzt werden. Es werden dann so viele blau umrandete Quadrate mit Spielsteinen besetzt wie abgebildet sind. Dabei ist unerheblich welche größe die Felder haben. Im Fall des Projekts \textsf{018} könnten auch die kleinen Felder verwendet werden, da nur drei Spielsteine pro Feld gesetzt werden. Die Aufteilung darf dabei nicht von der Abbildung abweichen. Befindet sich eine Lücke zwischen beiden blauen Quadraten, dann müssen die Felder auf deinen gebaut wird nicht benachbart sein. Im Zweifel wird die Bebauung auf der Spielkarte genauer beschrieben. Gibt es nicht ausgefüllte Quadrate in den blauen Quadraten, dann darf ein Feld mit bestimmter vorhandener Bebauung ausgebaut werden. Im gezeigten Projekt ist das nicht möglich und es müssen freie Felder neu bebaut werden.

In manchen Fällen gibt es zusätzliche Auswirkungen: Der Grundwasserspiegelspiegel könnte sinken \textsf{(GT)} oder die Biodiversität sich verändern \textsf{(B)}. Diese sind mit dem entsprechenden Vorzeichen gekennzeichnet und auf den Projektkarten näher beschrieben. Karten mit solchen zusätzlichen Auswirkungen werden in der Regel nach der Umsetzung nicht wieder auf den Ablagestapel sondern offen ausgelegt, da die Auswirkung nun für alle folgenden Runden gilt und Einfluss in der Wertungsphase hat. Ausnahmen sind auf den Karten entsprechend beschrieben. 

\newpage

# Spielphasen (Spielablauf) \label{phasen}

Das Spiel läuft in mehreren Phasen ab, und beginnt mit der *Wertungsphase* direkt nach der *Startaufstellung*. Die *Ereignis-*, *Planungs-* und *Umsetzungsphase* finden jede Runde in dieser Reihenfolge statt und schließen jeweils mit der *Wertungsphase* ab. 

## Startaufstellung

Die roten/grünen *Ereignis-* und *Projektkarten* werden verdeckt als 3 Stapel neben das Transformations-Spielbrett gelegt, welches wie in *Kapitel \ref{trans}* beschrieben vorbereitet werden kann.

Alle Spieler:innen können nun gemäß der aufgedruckten Startaufstellung Spielsteine für Wohnflächen (schwarz), Mobilität (rosa), Energie (gelb) und Vegetation (grün) auf die ausgewählten Stadtteil-Spielbretter verteilen. Bitte hier auch die Hinweise aus dem *Kapitel \ref{bretter}* beachten. 

Zusätzlich erhält jede Spieler:in ein Status-Spielbrett. Anschließend werden die Flächen und Spielsteine wie in *Kapitel \ref{status}* beschrieben gezählt und mit Spielsteinen markiert. Das Planungsbudget wird zu Beginn jeder Runde vor der Ereignisphase auf 10 gesetzt. Die aufgedruckte \textsf{Zählhilfe} wird im nun folgenden *Kapitel \ref{wertung}* näher beschrieben wird. 

## Wertungsphase \label{wertung}

In der Wertungsphase werden die Flächen und Spielsteine aller Spieler:innen zusammengezählt um das aktuelle Transformationslevel zu bestimmen. Auch zu Beginn des Spiels vor der ersten Ereignisphase wird die Wertungsphase einmal ausgeführt. Die Positionen der 5 Kategorien auf dem Transformations-Spielbrett werden wie folgt berechnet:

- **Versiegelung** $\textsf{(VS)}$ = Felder mit Wohnflächen \textsf{(W)} *plus* Felder mit regenerativer Energieerzeugung \textsf{(RE)} *minus* Mobilitäts-Felder \textsf{(M)} *minus* Freifelder \textsf{(F)} Beachtet die Hinweise in *Kapitel\ \ref{stadtteile}* und *\ref{status}*.
- **Vegetation** $\textsf{(V)}$ = Vegetations-Spielsteine \textsf{(V)} ohne Freifelder.
- **Grundwasser** $\textsf{(G)}$ = Ausgehend von der Position des Versiegelungsgrad \textsf{(VS)} *abzüglich* aller Effekte die sich durch Projekt- oder Ereigniskarten auf die Grundwassertiefe \textsf{(GT)} ergeben haben (vor der ersten Runde auf die gleiche Position wie $\textsf{VS}$ setzen).
- **Thermische Belastung** $\textsf{(T)}$ = Wohnflächen-Spielsteine \textsf{(W)} *minus* Vegetations-Spielsteine \textsf{(V)} *minus* Regenerative Engergie-Spielsteine \textsf{(RE)}.
    - hier zählen auch Vegetationsspielsteine oder Regenerative Energie-Spielsteine auf versiegelten Feldern (z.B. in Kombination mit Wohnungen).
    - Ohne Freiflächen, da es hier um das Mikroklima geht (bis 2 Meter über den Boden).
- **Biodiversiät** $\textsf{(B)}$ := Vegetationsspielsteine \textsf{(V)} *plus* Freifelder \textsf{(F)} *plus* Effekte die sich durch Projekt- oder Ereigniskarten auf die Grundwassertiefe \textsf{(GT)} ergeben haben.

\pic{./zählhilfe_4.png}{Zählhilfe für 4 Spieler:innen\label{hilfe}}

Die Zählhilfe kann nun wie folgt verwendet werden, um die Position für die Marker der 5 Kategorien zu bestimmen. Beispiel für Versiegelung \textsf{(VS)}: Setze einen beliebigen Spielstein auf die Postion **\textsf{0}** der Zählhilfe, frage nun der Reihe nach alle versiegelten Flächen \textsf{(W)} aller Spieler:innen ab und ziehe den Spielstein in **$\textsf{+}$**-Richtung auf der Zählhilfe in der ensprechenden Anzahl nacheinander nach. Frage nun alle Felder mit Mobilität \textsf{(M)} ab und ziehe den Spielstein in die entgegegengesetzt Richtung. Wiederhole das Prozedere mit den Feldern, die ausschließlich Vegetation \textsf{(V)} besitzen und jenen die überhaupt nicht belegt sind \textsf{(F)}. Füge nun wieder alle Felder mit Energieerzeugung \textsf{(RE)} in **$\textsf{+}$**-Richtung hinzu, die nicht schon mit Wohnfläche versiegelt worden sind. Jetzt kannst du die Position des Versiegelungsgrad auf der Zählhilfe ablesen (achte auf Beschriftungen wie $\textsf{0}{\gets}\textsf{VS}{\to}\textsf{1}$ und auf welcher Seite davon sich der Spielstein befindet mit dem du gezählt hast). Setze nun noch den schwarzen Spielstein der für das Transformations-Spielbrett bestimmt ist auf die ermittelte Position in der Reihe, die mit \textsf{Versiegelung} gekennzeichnet ist. Fahre nun mit den verbleibenden Kategorien entsprechend den Berechnungsregeln von oben fort.

\hyphenation{Trans-formations-level}

Sind nun alle Positionen markiert kann der Würfel für das Lichtenberger Transformationslevel gemäß des gewählten Schwierigkeitsgrades verschoben werden *(siehe Kapitel\ \ref{trans}* und *Abb.\ \ref{trans1})*. Verschiebt sich der Würfel auf „\textsf{GAME OVER}“, dann habt ihr die Transformation leider nicht geschafft. Vielleicht klappt es beim nächsten Mal. Andernfalls geht es weiter, sofern diese Wertungsphase nicht schon das Ende der 6. Runde markiert. Ist das Ende erreicht könnt ihr direkt zum *Kapitel\ \ref{ende}* springen.

### Rundenwechsel vorbereiten

Nun wird der Marker für die Planungsbudget aller Spieler:innen auf dem Status-Spielbrett wieder auf 10 gesetzt. 

Drehe nun den Würfel der auch das aktuelle Transformationslevel anzeigt so, dass die nächste Runde des Spiels nach oben zeigt. Liegt die erste Runde noch vor euch, dann dreht den Würfel auf die **$\textsf{1}$** (grüne Ziffer). Die Zahl in rot zeigt nun den Wohnbedarf an. Jede Spieler:in überprüft nun die Anzahl der Wohnflächen. Ist die Anzahl kleiner, muss entsprechend Planungsbudget wieder abgezogen werden *(siehe Kapitel\ \ref{wohnbedarf})*. Danach geht es mit der *Ereignisphase* weiter.

### Wohnbedarf \label{wohnbedarf}

Die Anzahl der Bevölkerung muss durch die Anzahl an Wohnflächen gedeckt sein (Wohnbedarf). Ist der Wohnbedarf zu Beginn einer Runde nicht erfüllt wird direkt nach der *Wertungsphase* und noch vor der *Planungsphase* das Planungsbudget um die gleiche Menge an fehlenden Wohnflächen für die entsprechende Spieler:in reduziert.
Zusätzlich wird Wohnungsmangel teilweise durch die Entstehung von Slums ausgeglichen. Die betreffenden Spieler:innen müssen dann **1** freies Feld mit **1** Wohnbebauung (schwarzer Spielstein) besetzen. Hat eine Spieler:in keine freien Flächen mehr, dann muss eine freie Fläche auf einem anderen Spielbrett besetzt werden. Stehen auf keinem der Spielfelder noch freie Felder zur Verfügung, dann muss ein Feld ohne vorhandener Wohnbebauung freigeräumt werden. Ist der Wohnungsmangel so groß, dass eine Spieler:in kein Planungsbudget für die kommende Rund hat, dann muss ein weiteres freies Feld mit einer Wohnbebauung bestetzt werden.

Der Wohnbedarf steigt in jeder Runde außer der letzten (6.) an: Von 7 in der ersten Runde auf 8 in der zweiten. Dann auf 10 in der dritten, auf 13 in der vierten bis zum Höchststand 17. In der sechsten und letzten Runde sinkt der Wohnbedarf wieder auf 15.
 
## Ereignisphase \label{ereignisse}

Ziehe 1 bis 3 Ereigniskarten und führe die beschriebenen Effekte sofort aus. Effekte gelten nur für die aktuelle Runde, es sei denn es wird auf einer Karte explizit anders beschrieben. Abhängig vom aktuellen Transformationslevel werden wie folgt Karten gezogen:

- Level **\textsf{--}**: **2** *rote* Ereigniskarten
- Level **\textsf{0}**: **2** *rote* und **1** *grüne* Ereigniskarte
- Level **\textsf{+}**: **1** *grüne* Ereigniskarte
- Level **\textsf{++}**: **1** *grüne* Ereigniskarte

<!--
> `TODO: Beispiel eines Ereignisses erklären`
-->

## Planungsphase \label{projekte}

Alle Spieler:innen ziehen nacheinander verdeckt jeweils 4 Projektkarten und entscheiden möglichst ohne fremde Hilfe, welche davon sie für je 1 Planungsressource in einer der folgenden Umsetzungsphasen behalten wollen. Nicht gewünschte Projekte werden verdeckt auf den Ablagestapel abgelegt. Es besteht die Möglichkeit, dass der Planungsprozess offen gespielt wird, wenn alle Spieler:innen damit einverstanden sind. Diskussionen verlängern unter Umständen das Spiel erhöhen aber auch die Wahrscheinlichkeit auf Erfolg. 

## Umsetzungsphase \label{umsetzung}

Die Umsetzungsphase ist die Hauptphase des Spiels. Jede Spieler:in kann maximal eine Aktion ausführen. Erst nach dem alle anderen Spieler:innen an der Reihe waren darf eine weitere Aktion der selben Spieler:in gespielt werden. Folgende Aktionen sind möglich:

**Abriss:** Es können jede Runde bis zu 4 Spielsteine für 2 Planungsressourcen abgerissen werden. Der Abriss kann auf mehrere Aktionen aufgeteilt werden, der Planungsaufwand wird aber nur einmal fällig. Werden weniger Spielsteine durch den Abriss vom Stadtteil-Spielbrett abgerissen müssen trotzdem 2 Planungsressourcen abgegeben werden.

**Projekttausch:** Projekte anderer Spieler:innen können für 2 Planungsressourcen pro Projekt jederzeit in der Umsetzungsphase getauscht werden. Spieler:innen können selbst entscheiden, wer dafür die zusätzlichen Planungskosten übernimmt (eine Aufteilung des Planungsaufwands ist ebenfalls möglich).

**Projekt umsetzen:** Wende für das gewünschte Projekt entsprechend das aufgedruckte Planungsbudget auf. Im Fall von Neubauprojekten müssen immer freie Felder besetzt werden, auch wenn noch Platz auf bereits bebauten Feldern ist, es sei denn das Projekt ermöglicht einen Ausbau direkt auf einem Feld mit vorhandener Bebauung.

<!--
> `TODO: Beispiel eines Projekts erklären`
-->

Nicht umgesetzte Projekte verbleiben auf der Hand der jeweiligen Spieler:in und können in einer späteren Runde gespielt werden. Nicht genutztes Planungsbudget verfällt am Ende der Runde und kann auch nicht anderen Spieler:innen zur Verfügung gestellt werden. Nutze dafür stattdessen die Möglichkeit des *Projekttauschs.*

## Spielende \label{ende}

Nach der letzten Runde oder nach einem fortzeitigen Ende nach der Wertungsphase darf über das Erreichte diskutiert werden. Was hat gefehlt um die Transformation zu schaffen oder welche Ereignisse waren besondere Herausforderungen um Lichtenberg in eine grüne Zukunft zu bewegen? Anregungen oder Kritik könnt ihr gerne an das Umweltbüro Lichtenberg richten.

\newpage

# Glossar

## Ereignisse

> `TODO`

## Projekte

> `TODO`

<!-- # Weiterführende Literatur -->
<!--
\rhead{\small{\textit{Weiterführende Literatur}}}
\addcontentsline{toc}{section}{Weiterführende Literatur}
\section*{Weiterführende Literatur}
-->


\small
