https://beta.dreamstudio.ai/dream 

model: Stable Diffusion v2.1-768
resolution = 512x512
cfg scale = 2 (default: 7)
steps = 30 (default)
seed = 2129253173 (best, final)

final prompt = dynamic pattern in (the) a grunge style with (light) neon paper tape: urban (city), bird trees shrubs solar panels (and) wind turbines trains bicycles

previous prompts:

solarpunk Lichtenberg, urban biodiversity transformation, heat wave, groundwater, neon tape grunge style, homogeneous composition

solarpunk transformation, urban biodiversity, heat waves, groundwater, trees shrubs solar panels and wind turbines, neon tape grunge style, uniform texture

neon tape DIY style, dynamic uniform pattern, urban, birds plants trees shrubs solar panels and wind turbines, buildings, bicycles

dark dynamic pattern in a grunge style, outlined with light neon paper tape: urban city, bird trees shrubs solar panels wind turbines trains bicycles


