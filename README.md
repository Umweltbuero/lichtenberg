# Readme: NEU-Lichtenberg

Ein Umweltbildungsspiel für Lichtenberg.

## Kurzbeschreibung

„NEU-Lichtenberg“ ist ein kooperatives Umweltbildungspiel für Lichtenberg. Ziel ist es die Transformation zu schaffen; d.h. Biodiversität trotz neuer Bedarfe durch Bevölkerungswachstum und Zuzug zu verbessern, eine stabile Grundwasserbilanz zu erreichen und die thermische Belastung zu reduzieren.

Jede Spieler:in ist für 2 bis 3 Stadtteile auf einem der 5 Spielbretter verantwortlich (siehe Anhang). Jedes Spielbrett hat unterschiedliche Ausgangsvorraussetzungen (Mangel und Überschüsse). Für den Spielerfolg wird aber alles zusammengerechnet.

Nach spätestens 6 Runden (im Jahr 2095) ist das Spiel vorbei, wenn eine oder mehrere Marker auf dem Transformationsspielbrett ihren Höchststand erreichen. In diesem Fall wäre das Spiel für alle verloren. Bleibt der Transformationsmarker nach 6 Runden im grünen Bereich ist das Spiel gewonnen und NEU-Lichtenberg kann in eine grüne Zukunft blicken.

Jede Runde werden zufällige Eregnisse von einem Kartenstapel gezogen (Ereignisphase). Je nach Transformationsgrad werden unterschiedliche viele gute oder schlechte Ereignisse gezogen, die die darauffolgende Umsetzungsphase beeinflussen. In der Planungsphase ziehen alle Spieler:innen verdeckt 4 Projektkarten, von denen soviele behalten werden können, wie Planungsbudget vorhanden ist (1 pro Karte). In der Umsetzungsphase können nun der Reihe nach die geplanten Projekte umgesetzt werden bis das Planungsbudget aufgebraucht ist. Ist das der Fall, dann folgt die Wertungssphase bei dem alle Spielsteine (Vegetation, Freiflächen, Wohnbebauung, Energiequellen, Mobilität) gezählt werden um die Marker (Versiegelung, Vegetation, Grundwasser, thermische Belastung und Biodiversität) auf dem Transformationsspielbrett entsprechend der Berechnungsregeln zu verschieben. Ist das Spiel dann noch nicht verloren oder gewonnen, beginnt die nächste Runde wieder mit der Ereignisphase.

Während der Ereignis- und Umsetzungsphase gibt es abhängig von den Ereignissen und Projekten immer wieder den Bedarf für Diskussionen und Kooperationen zwischen den Spieler:innen.
